const { user } = require('../models/user');

const createUserValid = (req, res, next) => {
  const payload = req.body;
  try {
    if (!payload.username || !(payload.password)
    ) {
      throw new Error("User entity to create isn't valid");
    } else {
      next();
    }
  } catch (err) {
    res.status(400).json({
      error: true,
      message: err.toString(),
    });
  }
};

exports.createUserValid = createUserValid;

