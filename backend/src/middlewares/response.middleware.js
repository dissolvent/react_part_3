const responseMiddleware = (req, res, next) => {
  if (Object.is(res.data, null)) {
    res.status(404).json({
      error: true,
      message: res.err ? res.err.toString() : new Error('Not found').toString(),
    });
  } else if (res.err) {
    res.status(400).json({
      error: true,
      message: res.err
        ? res.err.toString()
        : new Error('Bad request').toString(),
    });
  } else if (res.data) {
    res.status(200).json(res.data);
  } else {
    next();
  }
};

exports.responseMiddleware = responseMiddleware;
