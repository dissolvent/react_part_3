const { MessageRepository } = require('../repositories/messageRepository');
const { UserRepository } = require('../repositories/userRepository');

class MessageService {
  createMessage(payload) {
    const { userId: id, text } = payload;
    const user = UserRepository.getOne({ id });
    const message = {
      text,
      user: user.username,
      avatar: user.avatar,
      userId: user.id,
      editedAt: ''
    }
    const newMessage = MessageRepository.create(message);
    return newMessage;
  }

  search(search) {
    const message = MessageRepository.getOne(search);
    if (!message) {
      return null;
    }
    return message;
  }

  getAll() {
    const messages = MessageRepository.getAll();
    if (!messages) {
      return null;
    }
    return messages;
  }

  update(id, dataToUpdate) {
    if (!this.search(['id', id])) {
      throw new Error('Message not exists');
    }
    dataToUpdate.editedAt = new Date().toJSON();
    const messageUpdated = MessageRepository.update(id, dataToUpdate);
    if (!messageUpdated) {
      return null;
    }
    return messageUpdated;
  }

  delete(id) {
    const message = MessageRepository.delete(id);
    if (!message.length) {
      return null;
    }
    return message;
  }
}

module.exports = new MessageService();