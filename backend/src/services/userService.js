const { UserRepository } = require('../repositories/userRepository');

class UserService {
  createUser(user) {
    const newUser = UserRepository.create(user);
    return newUser;
  }

  search(search) {
    const user = UserRepository.getOne(search);
    if (!user) {
      return null;
    }
    return user;
  }

  getAll() {
    const users = UserRepository.getAll();
    if (!users) {
      return null;
    }
    return users;
  }

  update(id, dataToUpdate) {
    if (!this.search(['id', id])) {
      throw new Error('User not exists');
    }
    const userUpdated = UserRepository.update(id, dataToUpdate);
    if (!userUpdated) {
      return null;
    }
    return userUpdated;
  }

  delete(id) {
    const user = UserRepository.delete(id);
    if (!user.length) {
      return null;
    }
    return user;
  }
}

module.exports = new UserService();
