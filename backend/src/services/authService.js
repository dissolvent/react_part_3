const UserService = require('./userService');

class AuthService {
    login(userData) {
        const { username, password } = userData
        const user = UserService.search({ username });
        if(!user) {
            throw Error('User not found');
        }
        if (user.password === password) {
            return user;
        }
        throw Error('Password does not match');
    }
}

module.exports = new AuthService();
