const { Router } = require('express');
const UserService = require('../services/userService');
const {
  createUserValid,
} = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get(
  '/',
  (req, res, next) => {
    if (Object.keys(req.query).length) {
      res.err = new Error('Bad request');
      next();
    }
    try {
      const users = UserService.getAll();
      res.data = users;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  '/:id',
  (req, res, next) => {
    if (Object.keys(req.query).length) {
      res.err = new Error('Bad request');
      next();
    }
    try {
      const user = UserService.search(['id', req.params.id]);
      res.data = user;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.post(
  '/',
  (req, res, next) => {
    if (Object.keys(req.query).length) {
      res.err = new Error('Bad request');
    }
    next();
  },
  responseMiddleware,
  createUserValid,
  (req, res, next) => {
    try {
      const user = UserService.createUser(req.body);
      res.data = user;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.put(
  '/:id',
  (req, res, next) => {
    if (Object.keys(req.query).length) {
      res.err = new Error('Bad request');
    }
    next();
  },
  responseMiddleware,
  (req, res, next) => {
    try {
      const userUpToDate = UserService.update(req.params.id, req.body);
      res.data = userUpToDate;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.delete(
  '/:id',
  (req, res, next) => {
    if (Object.keys(req.query).length) {
      res.err = new Error('Bad request');
      next();
    }
    try {
      res.data = UserService.delete(req.params.id);
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
