const { Router } = require('express');
const MessagesService = require('../services/messagesService');
const {
  createUserValid,
} = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get(
  '/',
  (req, res, next) => {
    if (Object.keys(req.query).length) {
      res.err = new Error('Bad request');
      next();
    }
    try {
      const messages = MessagesService.getAll();
      res.data = messages;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  '/:id',
  (req, res, next) => {
    if (Object.keys(req.query).length) {
      res.err = new Error('Bad request');
      next();
    }
    try {
      const messages = MessagesService.search(['id', req.params.id]);
      res.data = messages;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.post(
  '/',
  (req, res, next) => {
    if (Object.keys(req.query).length) {
      res.err = new Error('Bad request');
    }
    next();
  },
  responseMiddleware,
  (req, res, next) => {
    try {
      const messages = MessagesService.createMessage(req.body);
      res.data = messages;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.put(
  '/:id',
  (req, res, next) => {
    if (Object.keys(req.query).length) {
      res.err = new Error('Bad request');
    }
    next();
  },
  responseMiddleware,
  (req, res, next) => {
    try {
      const userUpToDate = MessagesService.update(req.params.id, req.body);
      res.data = userUpToDate;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.delete(
  '/:id',
  (req, res, next) => {
    if (Object.keys(req.query).length) {
      res.err = new Error('Bad request');
      next();
    }
    try {
      res.data = MessagesService.delete(req.params.id);
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
