import React from 'react';
import {Route, Switch, Redirect } from 'react-router-dom';
import LoginPage from './components/LoginPage';
import Chat from './containers/Chat';
import Routing from './containers/Routing';

const App = () => (
    <div>
      <Routing />
      {/* <Switch>
        <Redirect exact from="/" to="/login" />
        <Route path="/login" exact component={LoginPage} />
        <Route path="/chat" exact component={Chat} />
      </Switch> */}
    </div>
);

export default App;
