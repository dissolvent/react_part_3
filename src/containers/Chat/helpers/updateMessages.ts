import { IFetchedMessages, IMessage } from '../types';

export const updateMessages = (fetchedMessages: IFetchedMessages[], currentUserId: string): IMessage[] => {
  if (!fetchedMessages.length) {
    return [];
  }
  const sorted = fetchedMessages.sort((a, b) => new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime());
  return sorted.map(message => ({
    ...message,
    likesCount: 0,
    canEdit: currentUserId === message.userId,
    myMessage: currentUserId === message.userId
  }));
};
