export interface ISendMessage {
  text: string
  userId: string
};
