import { put, call, takeEvery, all, select, take } from 'redux-saga/effects';
import { 
  fetchMessages,
  sendMessage,
  deleteMessage,
  fetchMessage,
  updateMessage
} from './service';

import * as types from './actionTypes'

export function* fetchMessagesSaga(userId) {
  try {
    const response = yield call(fetchMessages, userId);
    yield put({ type: types.SET_MESSAGES_SUCCESS, payload: response })
  } catch (error) {
    yield put({ type: types.SET_MESSAGES_ERROR, error });
  }
};

function* watchFetchMessages() {
  yield takeEvery(types.SET_MESSAGES, fetchMessagesSaga);
}

export function* setEditMessageSaga(id) {
  try {
    const response = yield call(fetchMessage, id);
    yield put({ type: types.EDIT_MESSAGE_SUCCESS, payload: response })
  } catch (error) {
    yield put({ type: types.EDIT_MESSAGE_ERROR, error });
  }
};

function* watchSetEditMessage() {
  yield takeEvery(types.EDIT_MESSAGE, setEditMessageSaga);
}

export function* updateMessageSaga(action) {
  try {
    const { userProfile: { id: userId } }  = yield select();
    const { id, text } = action.payload;
    yield call(updateMessage, { id, text });
    yield put({ type: types.UPDATE_MESSAGE_SUCCESS });
    yield put({ type: types.SET_MESSAGES, userId });
  } catch (error) {
    yield put({ type: types.UPDATE_MESSAGE_ERROR, error });
  }
};

function* watchUpdateMessage() {
  yield takeEvery(types.UPDATE_MESSAGE, updateMessageSaga);
}

function* sendMessageSaga(message) {
  try {
    const { userProfile: { id: userId } }  = yield select();
    const { text } = message;
    yield call(sendMessage, { text, userId });
    yield put({ type: types.SET_MESSAGES, userId });
  } catch (error) {
    yield put({ type: types.SEND_MESSAGE_ERROR, error });
  }
}

function* watchSendMessage() {
  yield takeEvery(types.SEND_MESSAGE, sendMessageSaga);
}

function* deleteMessageSaga(message) {
  try {
    const { userProfile: { id: userId } }  = yield select();
    const { id } = message;
    yield call(deleteMessage, { id });
    yield put({ type: types.SET_MESSAGES, userId });
  } catch (error) {
    yield put({ type: types.DELETE_MESSAGE_ERROR, error });
  }
}

function* watchDeleteMessage() {
  yield takeEvery(types.DELETE_MESSAGE, deleteMessageSaga);
}

export default function* chatSaga() {
  yield all([
    watchFetchMessages(),
    watchSendMessage(),
    watchDeleteMessage(),
    watchSetEditMessage(),
    watchUpdateMessage()
  ])
}
