import * as types from './actionTypes';


export const sendMessageAction = (text: string) => ({
  type: types.SEND_MESSAGE,
  text
});

export const fetchMessagesAction = (userId: string) => ({
  type: types.SET_MESSAGES,
  userId
});

export const deleteMessageAction = (id: string) => ({
  type: types.DELETE_MESSAGE,
  id
});

export const setEditMessage = (id: string) => ({
  type: types.EDIT_MESSAGE,
  id
});

export const updateMessage = (id, text) => ({
  type: types.UPDATE_MESSAGE,
  payload: { id, text }
})
