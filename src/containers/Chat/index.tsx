import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ChatHeader from '../../components/ChatHeader';
import MessageList from '../../components/MessageList';
import MessageInput from '../../components/MessageInput';
import Spinner from '../../components/Spinner';
import { 
  fetchMessagesAction,
  sendMessageAction,
  deleteMessageAction,
  setEditMessage
} from './actions';
import { IMessage } from './types';

import styles from './styles.module.scss';

type ChatProps = {
  messages?: IMessage[]
  userId: string
  isLoading: boolean
  loadMessages: (userId: string) => void
  sendMessage: (text: string) => void
  deleteMessage: (id: string) => void
  toggleEditMessage: (id: string) => void
}

const Chat: React.FC<ChatProps> = ({
  messages = [],
  userId,
  isLoading,
  loadMessages,
  sendMessage,
  deleteMessage,
  toggleEditMessage
}): JSX.Element => {
  useEffect(() => {
    if (isLoading) {
      loadMessages(userId);
    }
  }, [isLoading]);

  const lastMessage = () => {
    if (messages.length) {
      return messages ? messages[messages.length - 1].createdAt : '';
    }
    return '';
  };

  return (
    isLoading
      ? <Spinner />
      : (
        <div 
          className={styles.chatContainer} 
        >
          <ChatHeader
            name="My awesome chat"
            participants={23}
            messages={messages.length}
            lastMessage={lastMessage()}
          />
          <MessageList
            messages={messages}
            likeMessage={() => console.log('likeMessageAction')}
            deleteMessage={deleteMessage}
            toggleEdit={toggleEditMessage}
          />
          <MessageInput
            sendMessage={sendMessage}
          />
        </div>
      )
  );
};

// Chat.propTypes = {
//   messages: PropTypes.arrayOf(PropTypes.any),
//   isLoading: PropTypes.bool.isRequired,
//   inEditMessage: PropTypes.any,
//   toggleEditMessage: PropTypes.func.isRequired,
//   loadMessages: PropTypes.func.isRequired,
//   sendMessage: PropTypes.func.isRequired,
//   likeMessage: PropTypes.func.isRequired,
//   deleteMessage: PropTypes.func.isRequired,
//   editMessage: PropTypes.func.isRequired
// };

Chat.defaultProps = {
  messages: []
};

const mapStateToProps = state => ({
  messages: state.chat.messages,
  userId: state.userProfile.id,
  isLoading: state.chat.isLoading,
  inEditMessage: state.inEditMessage
});

const mapDispatchToProps = {
  loadMessages: fetchMessagesAction,
  toggleEditMessage: setEditMessage,
  sendMessage: sendMessageAction,
  deleteMessage: deleteMessageAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
