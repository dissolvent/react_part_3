import { updateMessages } from './helpers/updateMessages';

const api = 'http://localhost:3050/api/messages';

interface ISendMessage {
  text: string;
  username: string;
}

interface ISendMessageRequest {
  type: string;
  userId: string;
  message: ISendMessage;
}

interface IFetchMessages {
  userId: string
}

export const fetchMessages = async ({ userId }: IFetchMessages) => {
  const response = await fetch(api)
  const json = await response.json();
  if (!response.ok) {
    throw Error(json.message)
  }
  const updated = updateMessages(json, userId);

  return updated;
};

export const fetchMessage = async ({ id }) => {
  const response = await fetch(`${api}/${id}`)
  const json = await response.json();
  if (!response.ok) {
    throw Error(json.message)
  }
  return json;
};

export const sendMessage = async ({ userId, text }) => {
  const parameters = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ userId, text })
  };
  const response = await fetch(api, parameters)
  const json = await response.json();
  if (!response.ok) {
    throw Error(json.message)
  }
  return json;
};

export const updateMessage = async ({ id, text }) => {
  const parameters = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ text })
  };
  const response = await fetch(`${api}/${id}`, parameters)
  const json = await response.json();
  if (!response.ok) {
    throw Error(json.message)
  }
  return json;
};

export const deleteMessage = async ({ id }) => {
  const parameters = {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json'
    }
  };
  const response = await fetch(`${api}/${id}`, parameters)
  const json = await response.json();
  if (!response.ok) {
    throw Error(json.message)
  }
  return json;
};