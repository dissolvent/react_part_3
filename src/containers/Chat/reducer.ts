import * as types from './actionTypes';
import { IChatState } from './types';

const initialState: IChatState = {
  messages: [],
  isLoading: true
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.SET_MESSAGES_SUCCESS:
      return { ...state, messages: action.payload, isLoading: false };
      case types.SET_MESSAGES_ERROR:
        return { ...state, error: action.error, isLoading: false };
    case types.SEND_MESSAGE_SUCCESS:
      return { ...state, messages: [...(state.messages || []), action.payload] }
    case types.SEND_MESSAGE_ERROR:
        return { ...state, error: action.error, isLoading: false };
    case types.EDIT_MESSAGE:
        return { ...state, inEditMessage: undefined, isLoading: true }
    case types.EDIT_MESSAGE_SUCCESS:
      return { ...state, inEditMessage: action.payload, isLoading: false }
    case types.EDIT_MESSAGE_ERROR:
      return { ...state, error: action.error, isLoading: false };
    default: {
      return state;
    }
  }
};
