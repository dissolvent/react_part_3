import { IMessage } from './types';

export const SET_MESSAGES = 'SET_MESSAGES';
export const SET_MESSAGES_SUCCESS = 'SET_MESSAGES_SUCCESS'
export const SET_MESSAGES_ERROR = 'SET_MESSAGES_ERROR';

export const SEND_MESSAGE = 'SEND_MESSAGE';
export const SEND_MESSAGE_SUCCESS = 'SEND_MESSAGE_SUCCESS';
export const SEND_MESSAGE_ERROR = 'SEND_MESSAGE_ERROR';

export const EDIT_MESSAGE = 'EDIT_MESSAGE';
export const EDIT_MESSAGE_SUCCESS = 'EDIT_MESSAGE_SUCCESS';
export const EDIT_MESSAGE_ERROR = 'EDIT_MESSAGE_ERROR';

export const UPDATE_MESSAGE = 'UPDATE_MESSAGE';
export const UPDATE_MESSAGE_SUCCESS = 'UPDATE_MESSAGE_SUCCESS';
export const UPDATE_MESSAGE_ERROR = 'UPDATE_MESSAGE_ERROR';

export const DELETE_MESSAGE = 'DELETE_MESSAGE';
export const DELETE_MESSAGE_SUCCESS = 'DELETE_MESSAGE_SUCCESS';
export const DELETE_MESSAGE_ERROR = 'DELETE_MESSAGE_ERROR';

export interface ISetMessagesAction {
  type: typeof SET_MESSAGES
  messages: IMessage[]
}

export interface ISendMessageAction {
  type: typeof SEND_MESSAGE
  payload: IMessage
}

export interface ISetEditMessageAction {
  type: typeof EDIT_MESSAGE
  payload: IMessage | undefined
}

export type IActionTypes = ISetMessagesAction
  | ISendMessageAction
  | ISetEditMessageAction;
