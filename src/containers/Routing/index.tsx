import React, { useEffect } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import Spinner from '../../components/Spinner';
import Chat from '../../containers/Chat';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import PublicRoute from '../../components/PublicRoute';
import LoginPage from '../../components/LoginPage';
import PrivateRoute from '../../components/PrivateRoute';
import NotFound from '../../components/NotFound';
import EditMessage from '../../components/EditMessage';

interface IRoutingProps {
  isAuthorized: boolean;
  isLoading: boolean;
}

const Routing: React.FunctionComponent<IRoutingProps> = ({
  isAuthorized,
  isLoading
}) => {
  return (
    isLoading
      ? <Spinner />
      : (
        <div>
          <Header />
          <main>
            <Switch>
              <Redirect exact from="/" to="/login" />
              <PublicRoute exact path="/login" component={LoginPage} />
              <PrivateRoute exact path="/chat" component={Chat} />
              <PrivateRoute exact path="/edit" component={EditMessage} />
              <Route path="*" exact component={NotFound} />
            </Switch>
          </main>
          <Footer />
        </div>
      )
  )
}

const mapStateToProps = state => {
  const isAuthorized =  state.userProfile.isAuthorized;
  const isLoading = state.userProfile.isLoading;

  return {
    isAuthorized,
    isLoading
  };
}

export default connect(mapStateToProps)(Routing);
