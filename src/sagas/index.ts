import { all } from 'redux-saga/effects';
import authSaga from '../Authorization/sagas/authSaga';
import chatSagas from '../containers/Chat/sagas';
// import usersSagas from '../users/sagas';

export default function* rootSaga() {
    yield all([
        authSaga(),
        chatSagas()
    ]);
};
