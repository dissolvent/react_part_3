import { combineReducers } from "redux";
import chat from '../containers/Chat/reducer';
import userProfile from '../Authorization/reducer';
// import userPage from "../userPage/reducer";

const rootReducer = combineReducers({
    userProfile,
    chat
});

export default rootReducer;
