import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import Message from '../Message';

import { IMessage, MessageCallbacks } from '../../containers/Chat/types';

import styles from './styles.module.scss';

type MessageListProps = {
  messages: IMessage[]
} & MessageCallbacks;

const MessageList: React.FC<MessageListProps> = ({
  messages,
  likeMessage,
  deleteMessage,
  toggleEdit }): JSX.Element => {
  let previousDate: string | undefined;
  if (messages.length) {
    previousDate = messages[0].createdAt;
  }

  const chatBottom = useRef<HTMLDivElement>(null);
  useEffect(() => {
    if (chatBottom.current !== null) {
      chatBottom.current.scrollIntoView({ behavior: 'smooth' });
    }
  }, []);

  const compareDates = (nextDate: string): boolean => {
    if (!previousDate) {
      previousDate = nextDate;
      return false;
    }
    if (new Date(nextDate).getDate() !== new Date(previousDate).getDate()) {
      return true;
    }
    return false;
  };
  return (
    <div className={styles.messagesScreen}>
      {messages.map(message => {
        const { createdAt } = message;

        const isNextDay = compareDates(createdAt);
        const prev = previousDate;
        previousDate = createdAt;
        return (
          <div key={message.id} className={styles.messageWrap}>
            {isNextDay
              ? (
                <div className={styles.messageDateSeparator}>
                  {moment(prev).format('MMMM Do')}
                </div>
              )
              : null}
            <Message
              likeMessage={likeMessage}
              deleteMessage={deleteMessage}
              toggleEdit={toggleEdit}
              {...message}
            />
          </div>
        );
      })}
      <div ref={chatBottom} />
    </div>
  );
};

MessageList.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.any).isRequired,
  likeMessage: PropTypes.func.isRequired,
  deleteMessage: PropTypes.func.isRequired,
  toggleEdit: PropTypes.func.isRequired
};

export default MessageList;
