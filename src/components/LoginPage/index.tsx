import React, { useState, SyntheticEvent } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import styles from './styles.module.scss';
import { loginUserAction } from '../../Authorization/actions';

const LoginPage = ({ loginUser }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('')
  
  const handleUsernameInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    setUsername(event.target.value);
  }

  const handlePasswordInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(event.target.value);
  }
  const handleLogin = (event: SyntheticEvent) => {
    event.preventDefault();
    loginUser({ username, password });
  }

  return (  
    <div className={`container ${styles.loginContainer}`}>
      <div className={`col-md-6 ${styles.wrapper}`}>
        <form className={styles.loginForm} onSubmit={handleLogin}>
          <div className="form-group">
            <input 
              type="text" 
              className="form-control" 
              name="username" 
              placeholder="username"
              onChange={handleUsernameInput} />
          </div>
          <div className="form-group">
            <input 
              type="password"
              className="form-control"
              name="login"
              placeholder="password"
              onChange={handlePasswordInput} />
          </div>
          <div className="form-group">
            <input 
              type="submit"
              className={styles.btnSubmit}
              value="Log In"
            />
          </div>
        </form>
      </div>
    </div>
  )
}

const mapDispatchToProps = {
  loginUser: loginUserAction
};

export default connect(null, mapDispatchToProps)(LoginPage);
