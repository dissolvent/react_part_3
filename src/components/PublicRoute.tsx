import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const PublicRoute = ({ component: Component, isAuthorized, ...rest }) => (
  <Route
    {...rest}
    render={props => isAuthorized
      ? <Redirect to={{ pathname: '/chat', state: {from: props.location }}} />
      : <Component {...props} />
    }
  />
);

const mapStateToProps = state => ({
  isAuthorized: state.userProfile.isAuthorized
});

export default connect(mapStateToProps)(PublicRoute);
