import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
// import { authService } from '../Authorization/services/authService';

const PrivateRoute = ({ component: Component, isAuthorized, ...rest }) => (
  <Route 
    {...rest}
    render={props => (isAuthorized
      ? <Component {...props} />
      : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
      )}
  />
);

const mapStateToProps = state => ({
  isAuthorized: state.userProfile.isAuthorized
});

export default connect(mapStateToProps)(PrivateRoute);
