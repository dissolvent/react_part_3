import React from 'react';

import styles from './styles.module.scss';

const Spinner = (): JSX.Element => (
  <div className={styles.loader}>Loading...</div>
);

export default Spinner;
