import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import styles from './styles.module.scss';

export interface IChatHeader {
  name: string
  participants: number
  messages: number
  lastMessage: string
}

const ChatHeader = ({
  name,
  participants,
  messages,
  lastMessage
}: IChatHeader): JSX.Element => {
  const lastMessageAt = lastMessage.length
    ? `last message at ${moment(lastMessage).format('HH:mm')}`
    : 'No messages';

  return (
    <div className={styles.chatHeader}>
      <div className={styles.chatInfo}>
        <span>{name}</span>
        <span>
          {participants}
          {' '}
          participants
        </span>
        <span>
          {messages}
          {' '}
          messages
        </span>
      </div>
      <div className={styles.lastMessage}>
        <span>
          {lastMessageAt}
        </span>
      </div>
    </div>
  );
};

ChatHeader.propTypes = {
  name: PropTypes.string.isRequired,
  participants: PropTypes.number.isRequired,
  messages: PropTypes.number.isRequired,
  lastMessage: PropTypes.string.isRequired
};

export default ChatHeader;
