import React from 'react';
import styles from './styles.module.scss';

const Footer = () => (
  <footer className={styles.footer}>Copyright</footer>
);

export default Footer;
