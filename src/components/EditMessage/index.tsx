import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router';
import { connect } from 'react-redux';
import { updateMessage } from '../../containers/Chat/actions';

import styles from './styles.module.scss';
import Spinner from '../Spinner';

const EditMessage = ({
  message,
  isLoading,
  apply 
}) => {
  const [text, setText] = useState('');
  const history = useHistory();
  useEffect(() => {
    if (!isLoading && message) {
      setText(message.text);
    }
  }, [isLoading])

  const handleSave = () => {
    // TODO wait until response, redirect on success?
    apply(message.id, text);
    history.push('/chat');
  };

  const onCancel = () => {
    history.push('/chat');
  }

  return (
    isLoading 
      ? <Spinner />
      : <div className="modal-content">
          <div className="modal-body">
                <textarea
                  autoFocus
                  className={styles.messageText}
                  value={text}
                  placeholder="Write a message"
                  onChange={(ev: React.ChangeEvent<HTMLTextAreaElement>) => {
                    setText(ev.target.value);
                  }}
                />
              </div>
          <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={handleSave}
                >
                  Save changes
                </button>
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                  onClick={onCancel}
                >
                  Close
                </button>
              </div>
        </div>
  );
};

EditMessage.defaultProps = {
  message: {}
};

EditMessage.propTypes = {
  message: PropTypes.objectOf(PropTypes.any),
  isLoading: PropTypes.bool.isRequired,
  apply: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  message: state.chat.inEditMessage,
  isLoading: state.chat.isLoading
})

const mapDispatchToProps = {
  apply: updateMessage
}

export default connect(mapStateToProps, mapDispatchToProps)(EditMessage);
