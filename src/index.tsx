import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import App from './App';
import configureStore from './store/configureStore';

import './styles/reset.scss';
import './styles/common.scss';

const store = configureStore();

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>       
      <Router>
        <Route path="/" component={App}/>
      </Router>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root') as HTMLElement
);
