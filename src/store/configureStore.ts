import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer from "../reducers";
import rootSaga from '../sagas/index';
// import ReduxThunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
// import chatReducer from './containers/Chat/chatReducer';

// const composedEnhancers = composeWithDevTools(
//   applyMiddleware(ReduxThunk)
// );
export default function configureStore() {
  const sagaMiddleware = createSagaMiddleware();
  const store = createStore(
      rootReducer,
      composeWithDevTools(applyMiddleware(sagaMiddleware))
  );

  sagaMiddleware.run(rootSaga)

  return store;
}