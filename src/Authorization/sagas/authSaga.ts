import { put, call, takeEvery, all } from 'redux-saga/effects';
import { loginUser } from '../services/authService';

import * as types from '../actionTypes';

export function* loginSaga(payload) {
  try {
    const response = yield call(loginUser, payload);
    yield put({ type: types.LOGIN_USER_SUCCESS, response });
  } catch (error) {
    yield put({ type: types.LOGIN_USER_ERROR, error });
  }
}

function* watchLogin() {
  yield takeEvery(types.LOGIN_USER, loginSaga);
}

export default function* authSagas() {
  yield all([
    watchLogin(),
  ])
}