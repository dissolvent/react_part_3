import * as types from './actionTypes';

type State = {
  isAuthorized: boolean;
  isLoading: boolean;
};

const initialState: State = {
  isLoading: false,
  isAuthorized: false
};

export default function (state = initialState, action) {
  switch (action.type) {
      case types.LOGIN_USER_SUCCESS:
        return { ...state, ...action.response, isAuthorized: true };
      case types.LOGIN_USER_ERROR:
        return { ...state, ...action.error };
      default:
        return state;
  }
}
