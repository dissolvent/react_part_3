import { IUserLogin } from '../models/IUserLogin';
// import { post } from "../../helpers/requestHelper";
// import { setLocalStorageItem, getObjectFromLocalStorage } from "../../helpers/localStorageHelper";

interface ILoginRequest {
  type: string;
  user: IUserLogin;
}

export const loginUser = async (request: ILoginRequest) => {
  const LOGIN_API_ENDPOINT = 'http://localhost:3050/api/auth/login';

  const parameters = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(request.user)
  };
  const response = await fetch(LOGIN_API_ENDPOINT, parameters)
  const json = await response.json();
  if (!response.ok) {
    throw Error(json.message)
  }
  return json;
};
// export const login = async (body: IUserLogin) => {
//     return await post('auth/login', body);
// }

// export const isSignedIn = () => {
//     const user = getObjectFromLocalStorage('user');
//     return user ? true : false;
// };

// export const setLoginSession = (user: string) => {
//     setLocalStorageItem('username', user);
// }

// export const unsetLoginSession = () => {
//     setLocalStorageItem('username', null);
// }
